import typescript from "rollup-plugin-typescript2";
import sourceMaps from "rollup-plugin-sourcemaps";

const pkg = require("./package.json");
const PROD = process.env.NODE_ENV === "production";

export default (async () => {
  return {
    input: "src/game/index.ts",
    output: [
      { file: pkg.main, name: "ChainReaction", format: "umd", sourcemap: true },
      { file: pkg.module, format: "es", sourcemap: true },
    ],

    watch: {
      include: "src/**",
    },

    plugins: [
      typescript({
        useTsconfigDeclarationDir: true,
        tsconfigOverride: {
          compilerOptions: { declaration: PROD },
        },
      }),
      PROD && (await import("rollup-plugin-terser")).terser(),
      sourceMaps(),
    ],
  };
})();
