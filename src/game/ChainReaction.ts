import { Action, sleep } from "Util";

export type Renderer = (game: ChainReaction) => (removeCanvas: boolean) => void;

export interface Vector {
  x: number;
  y: number;
}

export interface GameState {
  field: Field;
  player: number;
  tilesOccupied: Array<Set<Cell>>;
  emptyTiles: Set<Empty>;
}

export type Strategy = (info: GameState) => AsyncIterableIterator<Vector>;

export interface Empty {
  player: null;
  pos: Vector;
  mass: 0;
  neighbors: Tile[];
  criticalMass: number;
}

export interface Cell {
  player: number;
  pos: Vector;
  mass: number;
  criticalMass: number;
  neighbors: Tile[];
}

export type Tile = Cell | Empty;

export type Field = Tile[][];

function matrix<T>(
  width: number,
  height: number,
  callback: (pos: Vector) => T,
): T[][] {
  return Array.from({ length: height }, (_, y) =>
    Array.from({ length: width }, (__, x) => callback({ x, y })),
  );
}

type DelayFactory = Action<Promise<any>>;
export type Delay = number | DelayFactory;

interface Options {
  size: Vector;
  numberOfPlayers: number;
  renderer: Renderer;

  onGameOver: (winner: number) => void;
  onError: (error: Error) => void;

  getInput: Strategy;
  chainDelay: Delay;
  turnDelay: Delay;
}

function outOfBounds(pos: Vector, bounds: Vector) {
  return pos.x < 0 || pos.x >= bounds.x || pos.y < 0 || pos.y >= bounds.y;
}

function createField(size: Vector): Empty[][] {
  const deltas = [
    { x: 0, y: 1 },
    { x: 1, y: 0 },
    { x: 0, y: -1 },
    { x: -1, y: 0 },
  ];

  const field = matrix<Empty>(size.x, size.y, pos => ({
    pos,
    neighbors: [],
    mass: 0,
    player: null,
    criticalMass: 0,
  }));
  for (let y = 0; y < field.length; y++) {
    for (let x = 0; x < field[y].length; x++) {
      const tile = field[y][x];
      tile.neighbors = deltas
        .map(pos => ({ x: pos.x + x, y: pos.y + y }))
        .filter(pos => !outOfBounds(pos, size))
        .map(pos => field[pos.y][pos.x]);
      tile.criticalMass = tile.neighbors.length;
    }
  }

  return field;
}

function isCritical(tile: Tile) {
  return tile.mass >= tile.criticalMass;
}

function createDelayFactory(x: number | DelayFactory): DelayFactory {
  return typeof x === "number" ? () => sleep(x) : x;
}

const MAX_CRITICAL_MASS = 4;

export default class ChainReaction {
  size: Vector;
  field: Field;
  tilesOccupied: Array<Set<Cell>>;
  emptyTiles: Set<Empty>;
  numberOfPlayers: number;
  currentPlayer: number = 0;
  gameOver: boolean = false;
  chainDelay: DelayFactory;
  turnDelay: DelayFactory;
  getInput: Strategy;
  unloadRenderer: (removeCanvas: boolean) => void;
  onError: (error: Error) => void;
  onGameOver: (winner: number) => void;

  constructor(opts: Options) {
    this.field = createField(opts.size);
    this.emptyTiles = new Set(
      (this.field as Empty[][]).reduce((acc, row) => acc.concat(row), []),
    );
    this.tilesOccupied = Array.from(
      { length: opts.numberOfPlayers },
      () => new Set(),
    );
    this.size = opts.size;
    this.numberOfPlayers = opts.numberOfPlayers;
    this.chainDelay = createDelayFactory(opts.chainDelay);
    this.turnDelay = createDelayFactory(opts.turnDelay);

    this.onGameOver = opts.onGameOver;
    this.onError = opts.onError;
    this.unloadRenderer = opts.renderer(this);
    this.getInput = opts.getInput;

    this.init();
  }

  static deserializeField(str: string): Field {
    const [settings, strTiles] = str.split(";");
    const [_, strX, strY] = settings.split(",");
    const size = {
      x: Number(strX),
      y: Number(strY),
    };

    const result: Field = createField(size);
    const tiles = strTiles.split(",").map(Number);

    for (let i = 0; i < tiles.length; i++) {
      const tile = tiles[i];
      const player = Math.floor(tile / MAX_CRITICAL_MASS);
      const mass = tile % MAX_CRITICAL_MASS;

      const x = i % size.x;
      const y = Math.floor(i / size.y);
      result[y][x].player = mass ? player : null;
      result[y][x].mass = mass;
    }

    return result;
  }

  async init() {
    let firstRound = this.numberOfPlayers;
    while (!this.gameOver) {
      if (
        firstRound <= 0 &&
        this.tilesOccupied[this.currentPlayer].size === 0
      ) {
        this.currentPlayer = this.nextPlayer();
        continue;
      }

      const success = await this.tryPlace().catch((err: Error) => {
        this.gameOver = true;
        this.onError(err);
        return false;
      });
      if (success) {
        await this.turnDelay();
        firstRound -= 1;
      }
    }
  }

  async tryPlace(oldGen?: AsyncIterableIterator<Vector>): Promise<boolean> {
    const gen =
      oldGen ||
      (await this.getInput({
        field: this.field,
        player: this.currentPlayer,
        tilesOccupied: this.tilesOccupied,
        emptyTiles: this.emptyTiles,
      }));

    const { value: pos, done } = await gen.next();
    if (done) {
      throw new Error("No input detected.");
    }
    const { x, y } = pos;
    if (outOfBounds(pos, this.size)) {
      throw new Error(`Position (${x}, ${y}) is out of bounds.`);
    }

    const tile = this.field[y][x];

    if (tile.mass === 0) {
      tile.player = this.currentPlayer;
      tile.mass = 1;
      this.tilesOccupied[this.currentPlayer].add(tile as Cell);
      this.currentPlayer = this.nextPlayer();
      this.emptyTiles.delete(tile as Empty);
      return true;
    }

    if (tile.player !== this.currentPlayer) {
      return this.tryPlace(gen);
    }

    tile.mass += 1;

    if (isCritical(tile)) {
      await this.explode(tile);
    }
    if (this.gameOver) {
      this.onGameOver(this.currentPlayer);
    }
    this.currentPlayer = this.nextPlayer();
    return true;
  }

  async explode(...cells: Cell[]) {
    const criticals = new Set<Cell>();
    while (cells.length) {
      const cell = cells.pop()!;
      for (const n of cell.neighbors) {
        n.mass += 1;
        if (n.player !== cell.player) {
          if (n.player !== null) {
            this.tilesOccupied[n.player].delete(n);
          } else {
            this.emptyTiles.delete(n);
          }
          n.player = cell.player;
          this.tilesOccupied[cell.player].add(n as Cell);
        }

        if (isCritical(n) && n.player !== null) {
          criticals.add(n);
        }
      }

      cell.mass -= cell.criticalMass;
      if (cell.mass === 0) {
        this.tilesOccupied[cell.player].delete(cell);
        const emptyCell: Empty = (cell as unknown) as Empty;
        emptyCell.player = null;
        this.emptyTiles.add(emptyCell);
      }

      if (!cells.length) {
        this.gameOver = this.tilesOccupied.every(
          (set, i) => i === this.currentPlayer || set.size === 0,
        );

        if (criticals.size && !this.gameOver) {
          await this.chainDelay();
          cells.push(...criticals);
          criticals.clear();
        }
      }
    }
  }

  nextPlayer() {
    return (this.currentPlayer + 1) % this.numberOfPlayers;
  }

  serializeField() {
    const initialValue =
      this.numberOfPlayers + "," + this.size.x + "," + this.size.y + ";";

    return this.field
      .reduce((acc, row) => acc.concat(row), [])
      .reduce((acc, tile, i) => {
        const prefix = i === 0 ? "" : ",";
        if (tile.player === null) {
          return acc + prefix + "0";
        }

        const encodedValue = tile.player * MAX_CRITICAL_MASS + tile.mass;
        return acc + prefix + encodedValue;
      }, initialValue);
  }
}
