import ChainReaction, { Field } from "./ChainReaction";

type Context = CanvasRenderingContext2D;
type ColorMap = string[];

function drawGrid(ctx: Context, tileSize: number) {
  const { width, height } = ctx.canvas;
  for (let x = tileSize; x < width; x += tileSize) {
    ctx.beginPath();
    ctx.moveTo(x, 0);
    ctx.lineTo(x, height);
    ctx.stroke();
  }
  for (let y = tileSize; y < height; y += tileSize) {
    ctx.beginPath();
    ctx.moveTo(0, y);
    ctx.lineTo(width, y);
    ctx.stroke();
  }
}

function drawField(
  ctx: Context,
  colorMap: ColorMap,
  field: Field,
  tileSize: number,
) {
  for (let y = 0; y < field.length; y++) {
    for (let x = 0; x < field[y].length; x++) {
      const tile = field[y][x];
      if (tile.player === null) {
        continue;
      }
      const startX = x * tileSize + tileSize / 2;
      const startY = y * tileSize + tileSize / 2;
      ctx.fillStyle = colorMap[tile.player];
      ctx.beginPath();
      ctx.arc(startX, startY, tileSize / 3, 0, Math.PI * 2);
      ctx.fill();
      ctx.fillStyle = "black";
      ctx.fillText(String(tile.mass), startX, startY);
    }
  }
}

export default function chainReactionCanvas(
  parent: HTMLElement,
  tileSize: number,
  playerColorMap: ColorMap,
  canvas = document.createElement("canvas"),
) {
  const ctx = canvas.getContext("2d")!;
  parent.appendChild(canvas);

  return function _chainReactionCanvas(game: ChainReaction) {
    const width = (canvas.width = game.size.x * tileSize);
    const height = (canvas.height = game.size.y * tileSize);
    let rafId = 0;
    canvas.style.width = width + "px";
    canvas.style.height = height + "px";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.font = `bold ${tileSize / 2}px sans-serif`;

    function loop() {
      const { field } = game;
      ctx.clearRect(0, 0, width, height);
      drawField(ctx, playerColorMap, field, tileSize);
      drawGrid(ctx, tileSize);
      rafId = requestAnimationFrame(loop);
    }

    rafId = requestAnimationFrame(loop);

    return (removeCanvas: boolean) => {
      requestAnimationFrame(() => cancelAnimationFrame(rafId));
      if (removeCanvas) {
        canvas.remove();
      }
    };
  };
}
