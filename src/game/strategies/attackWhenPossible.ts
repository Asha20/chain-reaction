import { isPrecritical, getChains, getChainMass, createStrategy } from "./util";

export default createStrategy(
  "attackWhenPossible",
  (minimumReward: number) =>
    async function*(state) {
      const { tilesOccupied, player, field } = state;
      const ownTiles = [...tilesOccupied[player]];
      const chains = getChains(field, true);

      const moves = ownTiles
        .filter(isPrecritical)
        .map(cell => {
          const reward = cell.neighbors
            .map(n => getChainMass(chains.get(n)!, [player]))
            .reduce((acc, x) => acc + x, 0);

          return { cell, reward };
        })
        .filter(({ reward }) => reward >= minimumReward)
        .sort((x1, x2) => x2.reward - x1.reward)
        .map(({ cell }) => cell.pos);

      yield* moves;
    },
);
