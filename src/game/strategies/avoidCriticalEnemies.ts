import { extendStrategy, isPrecritical } from "./util";

export default extendStrategy(
  "avoidCriticalEnemies",
  ({ field, player }) => pos => {
    return field[pos.y][pos.x].neighbors.every(
      neighbor => neighbor.player === player || isPrecritical(neighbor),
    );
  },
);
