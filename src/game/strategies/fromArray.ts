import { createStrategy } from "./util";
import { Vector } from "Game/ChainReaction";

export default createStrategy(
  "fromArray",
  (arr: Vector[]) =>
    async function*() {
      yield* arr;
    },
);
