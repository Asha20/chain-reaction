import playerInput from "./playerInput";
import playRandomly from "./playRandomly";
import preferCornersThenEdges from "./preferCornersThenEdges";
import avoidCriticalEnemies from "./avoidCriticalEnemies";
import preferBeingCritical from "./preferBeingCritical";
import attackWhenPossible from "./attackWhenPossible";
import fromArray from "./fromArray";
import multipleFromArray from "./multipleFromArray";

export {
  playerInput,
  playRandomly,
  preferCornersThenEdges,
  avoidCriticalEnemies,
  preferBeingCritical,
  attackWhenPossible,
  fromArray,
  multipleFromArray,
};
