import { repeat } from "Util";
import { Vector } from "Game/ChainReaction";
import fromArrayStrategy from "./fromArray";

export default (numberOfPlayers: number, moves: Vector[]) => {
  const initialValue: Vector[][] = repeat(numberOfPlayers, () => []);
  const movesForPlayers = moves.reduce((acc, move, i) => {
    const player = i % numberOfPlayers;
    acc[player].push(move);
    return acc;
  }, initialValue);

  return movesForPlayers.map(fromArrayStrategy);
};
