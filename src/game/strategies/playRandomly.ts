import { shuffleArray } from "Util";
import { createStrategy } from "./util";

export default createStrategy(
  "playRandomly",
  () =>
    async function*(state) {
      const { player, emptyTiles, tilesOccupied } = state;
      const availableTiles = shuffleArray([
        ...emptyTiles,
        ...tilesOccupied[player],
      ]);
      for (const tile of availableTiles) {
        yield tile.pos;
      }
    },
);
