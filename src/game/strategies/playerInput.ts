import { createStrategy } from "./util";
import { waitForEvent } from "Util";

export default createStrategy(
  "playerInput",
  (canvas: HTMLCanvasElement, tileSize: number) =>
    async function*() {
      while (true) {
        yield await waitForEvent(canvas, "click").then(e => {
          const x = Math.floor(e.offsetX / tileSize);
          const y = Math.floor(e.offsetY / tileSize);
          return { x, y };
        });
      }
    },
);
