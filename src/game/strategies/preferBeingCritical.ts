import { extendStrategy, isPrecritical } from "./util";

export default extendStrategy(
  "preferBeingCritical",
  ({ field, player }) => pos => {
    const tile = field[pos.y][pos.x];
    return tile.player === player && isPrecritical(tile);
  },
);
