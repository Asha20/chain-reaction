import { Field, Tile } from "../ChainReaction";
import { shuffleArray } from "Util";
import { createStrategy } from "./util";

function isAvailable(player: number, tile: Tile) {
  return tile.mass === 0 || tile.player === player;
}

function getCorners(field: Field): Tile[] {
  const width = field[0].length;
  const height = field.length;
  return [
    field[0][0],
    field[0][width - 1],
    field[height - 1][0],
    field[height - 1][width - 1],
  ];
}

function getEdges(field: Field): Tile[] {
  const edges: Tile[] = [];
  const width = field[0].length;
  const height = field.length;
  for (let x = 1; x < width - 1; x++) {
    edges.push(field[0][x]);
    edges.push(field[height - 1][x]);
  }
  for (let y = 1; y < height - 1; y++) {
    edges.push(field[y][0]);
    edges.push(field[y][width - 1]);
  }

  return edges;
}

export default createStrategy(
  "preferCornersThenEdges",
  () =>
    async function*(state) {
      const { field, player } = state;
      const corners = shuffleArray(getCorners(field));
      const edges = shuffleArray(getEdges(field));
      const cornersAndEdges = corners.concat(edges);

      for (const tile of cornersAndEdges) {
        if (isAvailable(player, tile)) {
          yield tile.pos;
        }
      }
    },
);
