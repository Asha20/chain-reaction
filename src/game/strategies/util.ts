import {
  Strategy,
  Vector,
  GameState,
  Field,
  Cell,
  Tile,
} from "../ChainReaction";
import { renameFunction } from "Util";

type Chain = Set<Cell>;

export function createStrategy<T extends (...args: any[]) => Strategy>(
  name: string,
  fn: T,
) {
  return renameFunction(name, (...args: Parameters<T>) => {
    const stringifiedArgs = JSON.stringify(args).slice(1, -1);
    const newName = name + "(" + stringifiedArgs + ")";

    return renameFunction<Strategy>(newName, fn(...args));
  });
}

export function extendStrategy(
  name: string,
  fn: (state: GameState) => (v: Vector) => boolean,
) {
  return function _extendStrategy(source: Strategy): Strategy {
    const newName = name + ", " + source.name;
    return renameFunction<Strategy>(newName, async function*(state) {
      const gen = source(state);
      const validPos = fn(state);

      const moves: Vector[] = [];
      for await (const pos of gen) {
        moves.push(pos);
        if (validPos(pos)) {
          yield pos;
        }
      }

      yield* moves;
    });
  };
}

export function isPrecritical(tile: Tile) {
  return tile.mass === tile.criticalMass - 1;
}

export function getChains(field: Field, ignorePlayer: boolean) {
  const cellToChain = new Map<Tile, Chain>();
  const visitedTiles = new Set<Tile>();
  const EMPTY_CHAIN: Chain = new Set();

  const shouldChain = (origin: Cell, candidate: Cell) =>
    ignorePlayer || origin.player === candidate.player;

  const getNeighbors = (cell: Cell) =>
    !isPrecritical(cell)
      ? []
      : cell.neighbors.filter(
          (n): n is Cell => n.player !== null && shouldChain(cell, n),
        );

  for (const row of field) {
    for (const tile of row) {
      if (tile.player === null) {
        cellToChain.set(tile, EMPTY_CHAIN);
        visitedTiles.add(tile);
        continue;
      }
      if (!isPrecritical(tile)) {
        cellToChain.set(tile, new Set([tile]));
        visitedTiles.add(tile);
        continue;
      }
      if (visitedTiles.has(tile)) {
        const existingChain = [...cellToChain.values()].find(c => c.has(tile))!;
        cellToChain.set(tile, existingChain);
        continue;
      }

      const chain: Chain = new Set([tile]);
      const tilesToVisit = getNeighbors(tile);

      while (tilesToVisit.length) {
        const t = tilesToVisit.pop()!;
        chain.add(t);
        visitedTiles.add(t);
        for (const n of getNeighbors(t).filter(x => !chain.has(x))) {
          chain.add(n);
          visitedTiles.add(n);
          tilesToVisit.push(n);
        }
      }

      cellToChain.set(tile, chain);
      visitedTiles.add(tile);
    }
  }

  return cellToChain;
}

export function concat(...strategies: Strategy[]): Strategy {
  return renameFunction<Strategy>(
    strategies.map(s => s.name).join(", "),
    async function*(state) {
      for (const strategy of strategies) {
        yield* strategy(state);
      }
    },
  );
}

export function getChainMass(chain: Chain, excludePlayers: number[] = []) {
  const excluding = new Set(excludePlayers);
  return [...chain].reduce((acc, x) => {
    return acc + (excluding.has(x.player) ? 0 : x.mass);
  }, 0);
}
