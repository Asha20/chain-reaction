import { repeat, Omit } from "Util";
import ChainReaction, {
  Vector,
  Renderer,
  Strategy,
  GameState,
  Delay,
} from "./ChainReaction";
import { Message } from "./strategyAnalyzerWorker";

// tslint:disable-next-line no-empty
const noRenderer: Renderer = () => () => {};

function getInput(strategies: Strategy[]) {
  return function _getInput(state: GameState) {
    return strategies[state.player](state);
  };
}

interface GameSettings {
  size: Vector;
  strategies: Strategy[];
  chainDelay?: Delay;
  turnDelay?: Delay;
}

type WorkerGameSettings = Omit<GameSettings, "chainDelay" | "turnDelay">;

function runGame(
  { size, strategies, chainDelay = 0, turnDelay = 0 }: GameSettings,
  renderer: Renderer,
): Promise<number> {
  return new Promise((resolve, reject) => {
    const game = new ChainReaction({
      chainDelay: renderer === noRenderer ? 0 : chainDelay,
      turnDelay: renderer === noRenderer ? 0 : turnDelay,
      numberOfPlayers: strategies.length,
      size,
      renderer,
      onGameOver: winner => {
        resolve(winner);
        game.unloadRenderer(false);
      },
      onError: err => {
        reject(err);
        game.unloadRenderer(false);
      },
      getInput: getInput(strategies),
    });
  });
}

type Tally = number[];

type Analysis = [() => void, Promise<Tally>];

export function analyze(
  amount: number,
  settings: GameSettings,
  onGameFinished?: (opts: { id: number; tally: Tally }) => void,
  renderer: Renderer = noRenderer,
): Analysis {
  const tally: Tally = repeat(settings.strategies.length, () => 0);
  let currentGameId: number = 0;
  const runInParallel = renderer === noRenderer;
  let active = true;

  // tslint:disable-next-line no-empty
  let cancel = (error?: Error) => {};

  const runGameAndReportResult = (
    _settings: GameSettings,
    _renderer: Renderer,
  ) => {
    currentGameId += 1;
    const ownId = currentGameId;
    return active
      ? runGame(_settings, _renderer)
          .then(winner => {
            if (active) {
              tally[winner] += 1;
              if (onGameFinished) {
                onGameFinished({ id: ownId, tally });
              }
            }
          })
          .catch(err => {
            err.message = `In game ${ownId}: ` + err.message;
            throw err;
          })
      : Promise.resolve();
  };

  // Run the games sequentially if a renderer is present.
  // If not, run them in parallel for faster execution.
  const tallyPromise = new Promise<Tally>(async (resolve, reject) => {
    cancel = (error?: Error) => {
      active = false;
      if (error) {
        reject(error);
      } else {
        resolve(tally);
      }
    };

    if (runInParallel) {
      await Promise.all(
        repeat(amount, () =>
          runGameAndReportResult(settings, renderer).catch(cancel),
        ),
      );
    } else {
      for (let i = 0; i < amount; i++) {
        await runGameAndReportResult(settings, renderer).catch(cancel);
      }
    }

    resolve(tally);
  });

  return [() => cancel(), tallyPromise];
}

export function analyzeInWorker(
  amount: number,
  { size, strategies }: WorkerGameSettings,
  onGameFinished?: (opts: { id: number; tally: Tally }) => void,
): Analysis {
  const worker = new Worker("./worker.js");
  let currentTally: number[];
  // tslint:disable-next-line no-empty
  let cancel: () => void = () => {};

  const tally = new Promise<Tally>((resolve, reject) => {
    const stringifiedStrategies = JSON.stringify(strategies.map(s => s.name));

    cancel = () => {
      worker.terminate();
      resolve(currentTally);
    };

    worker.addEventListener("message", e => {
      const data = e.data as Message<{ id: number; tally: Tally }>;

      if (data.type === "next" && onGameFinished) {
        currentTally = data.value.tally;
        return onGameFinished(data.value);
      }
      if (data.type === "error") {
        return reject(new Error(data.error));
      }
      if (data.type === "complete") {
        return resolve(currentTally);
      }
    });
    worker.addEventListener("error", error => reject(error));

    worker.postMessage({
      amount,
      size,
      strategies: stringifiedStrategies,
    });
  });

  return [cancel, tally];
}
