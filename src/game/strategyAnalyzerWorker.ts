import { analyze } from "./strategyAnalyzer";
import { Vector, Strategy } from "./ChainReaction";
import * as strategies from "./strategies";
import { concat } from "./strategies/util";

const globalObject = (self as unknown) as DedicatedWorkerGlobalScope;

type ValidStrategyName = keyof typeof strategies;

interface Data {
  amount: number;
  size: Vector;
  strategies: string;
}

export type Message<T, E = any> =
  | { type: "next"; value: T }
  | { type: "error"; error: E }
  | { type: "complete" };

function sendMessage<T>(type: "next", value: T): void;
function sendMessage<E extends any>(type: "error", error: E): void;
function sendMessage(type: "complete"): void;
function sendMessage<T>(type: "next" | "error" | "complete", valueOrError?: T) {
  switch (type) {
    case "next":
      return globalObject.postMessage({ type, value: valueOrError });
    case "error":
      return globalObject.postMessage({ type, error: valueOrError });
    case "complete":
      return globalObject.postMessage({ type });
  }
}

const validStrategyNameRegex = /^(\w+)\((.*)\)$/;

function deserializeStrategy(str: string): Strategy {
  const concatedStrategies = str.split(", ");
  if (concatedStrategies.length > 1) {
    return concat(...concatedStrategies.map(deserializeStrategy));
  }

  const match = str.match(validStrategyNameRegex);
  if (!match) {
    throw new Error(`Invalid serialization: ${str}`);
  }

  const name = match[1];

  if (!(name in strategies)) {
    throw new Error(`Strategy ${name} does not exist.`);
  }
  const args = JSON.parse("[" + match[2] + "]");

  const strategyFactory = strategies[name as ValidStrategyName] as (
    ...args: any
  ) => Strategy;

  const strategy = strategyFactory(...args);
  return strategy;
}

globalObject.addEventListener("message", e => {
  const data = e.data as Data;

  const serializedStrategies = JSON.parse(data.strategies) as string[];
  const deserializedStrategies = serializedStrategies.map(deserializeStrategy);

  const settings = {
    strategies: deserializedStrategies,
    size: data.size,
  };

  const [_, tally] = analyze(data.amount, settings, x => {
    sendMessage("next", x);
  });

  tally
    .then(() => sendMessage("complete"))
    .catch(err => sendMessage("error", err.message));
});
