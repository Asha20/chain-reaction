// tslint:disable-next-line no-console
export const log = console.log;

export type AnyFunction = (...args: any[]) => any;

interface ObjectLiteral {
  [key: string]: any;
}

export const query = location.search
  .slice(1)
  .split("&")
  .map(pair => pair.split("="))
  .reduce<ObjectLiteral>((acc, [key, value]) => {
    const decoded = decodeURIComponent(value);
    const num = Number(decoded);
    acc[key] = Number.isNaN(num) ? decoded : num;
    return acc;
  }, {});

export const gameOptions = <T extends ObjectLiteral>(defaults: T): T => {
  const queryOpts = query.options ? JSON.parse(query.options) : {};

  return typeof queryOpts === "object"
    ? Object.assign({}, defaults, queryOpts)
    : {};
};

export function repeat<T>(n: number, fn: (index: number) => T): T[] {
  return Array.from({ length: n }, (_, i) => fn(i));
}

export function removeFromArray<T>(array: T[], item: T): T[] {
  const index = array.indexOf(item);
  array.splice(index, 1);
  return array;
}

export function shuffleArray<T>(array: T[]): T[] {
  let temp;
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
}

// tslint:disable-next-line ban-types
export function renameFunction<T extends Function>(name: string, fn: T): T {
  return Object.defineProperty(fn, "name", {
    value: name,
  });
}

export function omit<T extends any, K extends string[]>(
  obj: T,
  ...keys: K
): Omit<T, K[number]> {
  const forbiddenKeys = new Set(keys);
  return Object.keys(obj).reduce<any>((acc, key) => {
    if (!forbiddenKeys.has(key)) {
      acc[key] = obj[key];
    }
    return acc;
  }, {});
}

export function sleep(ms: number): Promise<void> {
  return ms <= 0
    ? Promise.resolve()
    : new Promise(resolve => setTimeout(resolve, ms));
}

export function waitForEvent<K extends keyof HTMLElementEventMap>(
  element: HTMLElement,
  event: K,
) {
  return new Promise<HTMLElementEventMap[K]>(resolve => {
    element.addEventListener(event, e => resolve(e), { once: true });
  });
}

export type Omit<T, U> = Pick<T, Exclude<keyof T, U>>;
export type Action<T extends any> = () => T;
